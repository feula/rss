package fr.eula.rss

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jakewharton.threetenabp.AndroidThreeTen
import com.squareup.sqldelight.android.AndroidSqliteDriver
import fr.eula.rss.homescreen.HomescreenFragment
import fr.eula.rss.repository.Repository

class MainActivity : AppCompatActivity() {

    internal var repository: Repository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidThreeTen.init(this)
        setContentView(R.layout.main_activity)

        // Reinstantiate our repository as needed
        val driver = AndroidSqliteDriver(Database.Schema, this, "rss.db")
        val database = Database(driver)
        repository = Repository(database)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, HomescreenFragment.newInstance())
                    .commitNow()
        }
    }
}