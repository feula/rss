package fr.eula.rss.utils

import java.lang.IllegalStateException

/**
 * Represents either a success (when Right) or a failure (when Left). This allows us to convey
 * more information with just a single return type.
 */
sealed class Either<out L, out R> {
    data class Left<out L>(val a: L): Either<L, Nothing>()
    data class Right<out R>(val b: R): Either<Nothing, R>()
}

/** Converts an object T into an Either.Left<T> */
fun <T> T.left(): Either.Left<T> = Either.Left(this)
/** Converts an object T into an Either.Right<T> */
fun <T> T.right(): Either.Right<T> = Either.Right(this)

fun <L, R> Either<L, R>.isLeft() = this is Either.Left<L>
fun <L, R> Either<L, R>.isRight() = this is Either.Right<R>

/** Attempts to unwrap an object from its Either wrapper. This is unsafe and will throw an exception
 * if this either is an Either.Left */
fun <L, R> Either<L, R>.unwrap(): R = when (this) {
    is Either.Left -> throw IllegalStateException("Cannot unwrap an instance of Either.Left")
    is Either.Right -> this.b
}

/** Applies the given functions to the contents of the Either and outputs a final unwrapped value */
fun <L, R, O> Either<L, R>.fold(left: (L) -> O, right: (R) -> O) = when (this) {
    is Either.Left -> left(this.a)
    is Either.Right -> right(this.b)
}

/** Applies the given transformation to this Either, if it is an Either.Right, or does nothing. */
fun <L, R, O> Either<L, R>.map(transform: (R) -> O) = when (this) {
    is Either.Left -> this
    is Either.Right -> transform(this.b).right()
}

