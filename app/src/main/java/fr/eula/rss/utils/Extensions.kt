package fr.eula.rss.utils

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

/**
 * Begins a suspending GET request with Volley, blocking the coroutine until it returns either
 * an error or the data.
 */
suspend fun RequestQueue.suspendGet(url: String): Either<VolleyError, String> =
    suspendCancellableCoroutine { continuation ->
        val request = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                continuation.resume(response.right())
            }, Response.ErrorListener { err ->
                continuation.resume(err.left())
            } )

        this.add(request)
    }
