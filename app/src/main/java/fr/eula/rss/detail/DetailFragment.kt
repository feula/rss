package fr.eula.rss.detail

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import fr.eula.rss.MainActivity
import fr.eula.rss.R
import fr.eula.rss.databinding.DetailFragmentBinding
import org.threeten.bp.format.DateTimeFormatter
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException

class DetailFragment : BottomSheetDialogFragment() {
    companion object {
        private const val KEY_ARTICLE_ID = "article_id"
        fun newInstance(itemId: String) = DetailFragment().apply {
            arguments = Bundle().apply {
                putString(KEY_ARTICLE_ID, itemId)
            }
        }
    }
    private lateinit var viewModel: DetailVM
    private var _binding: DetailFragmentBinding? = null
    private val binding get() = _binding!! // Only valid during the appropriate lifetimes.

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val repository = (requireActivity() as MainActivity).repository
                ?: throw IllegalStateException("Could not fetch the repository from the activity")
        val articleId = arguments?.getString(KEY_ARTICLE_ID)
                ?: throw IllegalArgumentException("$KEY_ARTICLE_ID not passed, this argument is mandatory")

        viewModel = ViewModelProvider(this, DetailVMFactory(articleId, repository)).get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = DetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // We want to force the bottom sheet to be expanded by default, so as to not hide buttons
        dialog.setOnShowListener {
            val bottomSheetDialog = dialog as BottomSheetDialog
            bottomSheetDialog.findViewById<FrameLayout>(R.id.design_bottom_sheet)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()

        viewModel.noSuchArticle.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), R.string.no_such_article, Toast.LENGTH_LONG).show()
        })

        viewModel.article.observe(viewLifecycleOwner, Observer {  news ->
            binding.detailArticleAuthorAndDate.text = getString(R.string.article_author_and_date, news.author,
                news.updated.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
            binding.detailArticleContent.text = news.content
            binding.detailArticleTitle.text = news.title
            binding.detailArticleImage.isVisible = news.imageUrl != null
            if (news.imageUrl != null) {
                Glide.with(requireContext())
                    .load(news.imageUrl)
                    .into(binding.detailArticleImage)
            }
        })

        // We could leave the default behavior of closing on back and tapping outside the dialog,
        // but a close button on the dialog simply looks good.
        binding.detailArticleClose.setOnClickListener {
            parentFragmentManager.popBackStack()
        }

        // Opening the actual link should be handled by a browser, not us.
        binding.detailArticleVisit.setOnClickListener {
            val article = viewModel.article.value ?: return@setOnClickListener

            Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(article.link)
                startActivity(this)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}