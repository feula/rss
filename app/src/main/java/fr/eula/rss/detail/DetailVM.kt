package fr.eula.rss.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import fr.eula.rss.parser.RSSItem
import fr.eula.rss.repository.Repository
import fr.eula.rss.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class DetailVMFactory(private val articleId: String, private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(String::class.java, Repository::class.java)
            .newInstance(articleId, repository)
    }
}

class DetailVM(articleId: String, private val repository: Repository) : ViewModel() {
    private val _noSuchArticle = SingleLiveEvent<Unit>()
    val noSuchArticle: LiveData<Unit> = _noSuchArticle

    private val _article = SingleLiveEvent<RSSItem>()
    val article: LiveData<RSSItem> = _article

    init {
        viewModelScope.launch {
            val found = repository.getOne(articleId)
            if (found == null) {
                _noSuchArticle.call()
                return@launch
            }

            _article.postValue(found)
        }
    }
}