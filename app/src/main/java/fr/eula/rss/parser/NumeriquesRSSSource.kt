package fr.eula.rss.parser

import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import fr.eula.rss.utils.*
import org.threeten.bp.ZonedDateTime
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.StringReader
import java.lang.Exception

class NumeriquesRSSFetcher(private val queue: RequestQueue): RSSFetcher {
    private val url: String = "https://www.lesnumeriques.com/rss.xml"
    override suspend fun fetch(): Either<VolleyError, String> {
        return queue.suspendGet(url)
    }
}

class NumeriquesRSSTransformer(): RSSTransformer {
    // NOTE: the link to the image may be present in an <img> tag inside the content. Don't forget to parse this too.
    // This can easily be extracted with a regex, thankfully.

    /**
     * Transforms a source input from les numériques into a list of RSS items.
     * Note that this assumes that the RSS is properly formed, and will not report errors or malformed
     * RSS.
     */
    override fun transform(input: String): List<RSSItem> {
        val factory = XmlPullParserFactory.newInstance()
        factory.isNamespaceAware = false // RSS doesn't do namespaces.
        val parser = factory.newPullParser()
        val items = mutableListOf<RSSItem>()

        return try {
            parser.setInput(StringReader(input))
            parseDocument(parser, items)

            items
        } catch (ex: Exception) {
            // The parser might be very unhappy about the RSS we give it. code in parseDocument should
            // normally never throw.
            // In that case, the error is swallowed, although we could return an Either from it too.
            emptyList()
        }
    }

    /**
     * Parses an entire RSS document, and mutates the given `items` parameters when items are found
     */
    private fun parseDocument(parser: XmlPullParser, items: MutableList<RSSItem>) {
        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG && parser.name == "feed") {
                readFeed(parser, items)
            }
            eventType = parser.next()
        }
    }

    /**
     * Parses an RSS document starting from the <feed> tag. The given parser should be at this position
     * in the document (after having scanned the tag)
     */
    private fun readFeed(parser: XmlPullParser, items: MutableList<RSSItem>) {
        var eventType = parser.eventType
        var isEndOfFeed = false
        while (!isEndOfFeed) {
            if (eventType == XmlPullParser.START_TAG && parser.name == "entry") {
                readEntry(parser, items)
            }
            eventType = parser.next()
            isEndOfFeed = parser.name == "feed" && (eventType == XmlPullParser.END_TAG || eventType == XmlPullParser.END_DOCUMENT)
        }
    }

    /**
     * Parses a single <entry> tag, and mutates `items` to add the parsed RssItem to it. If this tag is
     * malformed, it will not mutate it.
     */
    private fun readEntry(parser: XmlPullParser, items: MutableList<RSSItem>) {
        var eventType = parser.eventType
        var isEndOfEntry = parser.name == "entry" && (eventType == XmlPullParser.END_TAG || eventType == XmlPullParser.END_DOCUMENT)
        var title: String? = null
        var link: String? = null
        var id: String? = null
        var updated: ZonedDateTime? = null
        var summary: String? = null
        var author: String? = null
        var content: String? = null
        var imageUrl: String? = null

        while (!isEndOfEntry) {
            // Let's only parse when we enter the tag
            if (eventType == XmlPullParser.START_TAG) {
                when (parser.name) {
                    "title" -> title = getInnerText(parser)
                    "link" -> link = parser.getAttributeValue("", "href")
                    "id" -> id = getInnerText(parser)
                    "updated" -> updated = getInnerText(parser)
                            ?.takeIf { it.isNotEmpty() }
                            ?.let { ZonedDateTime.parse(it) }
                            ?: ZonedDateTime.now()
                    "author" -> author = parseAuthor(parser)
                    "content" -> content = getInnerText(parser)
                    "summary" -> summary = getInnerText(parser)
                }
            }
            eventType = parser.next()
            isEndOfEntry = parser.name == "entry" && (eventType == XmlPullParser.END_TAG || eventType == XmlPullParser.END_DOCUMENT)

        }

        imageUrl = content?.let { attemptImageUrlExtraction(it) }

        if (title != null && link != null && id != null && updated != null && summary != null
            && author != null && content != null) {
            items.add(RSSItem(title, link, id, updated, summary, author, content, imageUrl))
        }
    }

    private fun getInnerText(parser: XmlPullParser): String? {
        // This assumes that when we enter this method, we're at the start of a node, and that it
        // only contains text inside, no other nodes
        // If it doesn't, well, that'll break most of our parsing.
        // We do not need to do a second .next() and trust whoever is calling us to do it.
        if (parser.eventType != XmlPullParser.TEXT) {
            parser.next()
        }
        return parser.text?.trim()
    }

    /**
     * Parses a single <author> tag, since the name is actually contained in a <name> tag inside. This
     * extracts it.
     */
    private fun parseAuthor(parser: XmlPullParser): String? {
        var eventType = parser.eventType
        var name: String? = null
        var isEndOfAuthor = parser.name == "author" && (eventType == XmlPullParser.END_TAG || eventType == XmlPullParser.END_DOCUMENT)
        while (!isEndOfAuthor) {
            if (parser.name == "name") {
                name = getInnerText(parser)
                break
            }
            eventType = parser.next()
            isEndOfAuthor = parser.name == "author" && (eventType == XmlPullParser.END_TAG || eventType == XmlPullParser.END_DOCUMENT)
        }

        return name
    }

    /**
     * The image for a single post is contained in an <img> tag inside the description. This attempts
     * to extract any matching pattern from the given source.
     */
    private fun attemptImageUrlExtraction(source: String): String? {
        // Kotlin always considers the entire match as a group, so the first matching group will
        // actually be at index 1.
        return Regex("<img src=\"(.*)\".*\\/>").find(source)
                ?.groups
                ?.get(1)
                ?.value
    }
}


