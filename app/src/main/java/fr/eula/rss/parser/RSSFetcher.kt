package fr.eula.rss.parser

import com.android.volley.VolleyError
import fr.eula.rss.utils.Either
import org.threeten.bp.ZonedDateTime

/**
 * A classic RSSItem with the appropriate fields, with two changes to the spec:
 *   - the author is simply saved as a String, as we do not need more metadata
 *   - an image url can be saved too.
 */
data class RSSItem(val title: String, val link: String, val id: String, val updated: ZonedDateTime,
                   val summary: String, val author: String, val content: String, val imageUrl: String?)

interface RSSFetcher {
    suspend fun fetch(): Either<VolleyError, String>
}