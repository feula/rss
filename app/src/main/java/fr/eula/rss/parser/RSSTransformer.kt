package fr.eula.rss.parser

interface RSSTransformer {
    fun transform(input: String): List<RSSItem>
}