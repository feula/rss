package fr.eula.rss.parser

import com.android.volley.VolleyError
import fr.eula.rss.homescreen.FeedItem
import fr.eula.rss.utils.Either
import fr.eula.rss.utils.map

class RSSSource(private val parser: RSSFetcher, private val transformer: RSSTransformer) {
    suspend fun get(): Either<VolleyError, List<RSSItem>> {
        val source = parser.fetch()
        val items = source.map { transformer.transform(it) }

        return items
    }
}