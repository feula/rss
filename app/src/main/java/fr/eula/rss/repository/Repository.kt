package fr.eula.rss.repository

import fr.eula.rss.Database
import fr.eula.rss.Rss_items
import fr.eula.rss.parser.RSSItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class Repository(private val database: Database) {
    suspend fun getAll() = withContext(Dispatchers.IO) {
        database.rssItemQueries
            .all()
            .executeAsList()
            .map(Rss_items::intoApp)
    }

    suspend fun getOne(id: String) = withContext(Dispatchers.IO) {
        database.rssItemQueries
            .one(id)
            .executeAsOneOrNull()
            ?.intoApp()
    }

    suspend fun save(items: List<RSSItem>) = withContext(Dispatchers.IO) {
        database.rssItemQueries.transaction {
            items.forEach {
                database.rssItemQueries.insert(it.intoDatabase())
            }
        }
    }
}

private fun Rss_items.intoApp() = this.let { rss -> RSSItem(rss.title, rss.link, rss.id,
    ZonedDateTime.ofInstant(Instant.ofEpochSecond(rss.updated), ZoneId.systemDefault()),
    rss.summary, rss.author, rss.content, rss.image_url) }

private fun RSSItem.intoDatabase() = this.let { rss -> Rss_items.Impl(rss.id, rss.title, rss.link,
    rss.updated.toEpochSecond(), rss.summary, rss.author, rss.content, rss.imageUrl) }