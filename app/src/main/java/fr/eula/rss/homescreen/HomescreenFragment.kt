package fr.eula.rss.homescreen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import fr.eula.rss.MainActivity
import fr.eula.rss.R
import fr.eula.rss.databinding.HomescreenFragmentBinding
import fr.eula.rss.detail.DetailFragment
import fr.eula.rss.parser.NumeriquesRSSFetcher
import fr.eula.rss.parser.NumeriquesRSSTransformer
import fr.eula.rss.parser.RSSSource
import java.lang.IllegalStateException

class HomescreenFragment : Fragment() {

    companion object {
        fun newInstance() = HomescreenFragment()
    }

    private lateinit var viewModel: HomescreenVM
    private var _binding: HomescreenFragmentBinding? = null
    private val binding get() = _binding!! // Only valid during the appropriate lifetimes.


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = HomescreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val queue = Volley.newRequestQueue(requireActivity())
        val repository = (requireActivity() as MainActivity).repository
            ?: throw IllegalStateException("Could not acquire repository from activity")
        val source = RSSSource(NumeriquesRSSFetcher(queue), NumeriquesRSSTransformer())

        viewModel = ViewModelProvider(this, HomescreenVMFactory(source, repository)).get()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.homescreenContentList.adapter = FeedAdapter(this::onItemClickNavigate)
        binding.homescreenContentList.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.homescreenContentList.addItemDecorationOnce(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
    }

    override fun onResume() {
        super.onResume()

        viewModel.feedArticles.observe(viewLifecycleOwner, Observer { items ->
            (binding.homescreenContentList.adapter as FeedAdapter).submitList(items)
        })

        var notificationSnackbar: Snackbar? = null
        viewModel.userNotification.observe(viewLifecycleOwner, Observer { message ->
            notificationSnackbar?.dismiss()
            message?.let {
                notificationSnackbar = Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
                notificationSnackbar?.show()
            }
        })
    }

    private fun onItemClickNavigate(itemId: String) {
        parentFragmentManager.beginTransaction()
            .add(DetailFragment.newInstance(itemId), "detail")
            .addToBackStack("homescreen")
            .commit()
    }
}

/**
 * Inserts an item decoration in a recyclerview, ensuring that two instances of the same item decoration
 * will never be added. This doesn't require you to keep a reference to the one that was added at the
 * time.
 */
private fun RecyclerView.addItemDecorationOnce(decoration: RecyclerView.ItemDecoration) {
    var atIndex: Int? = null
    for (i in 0 until itemDecorationCount) {
        if (getItemDecorationAt(i).javaClass == decoration.javaClass) {
            atIndex = i
            break
        }
    }

    atIndex?.let { removeItemDecorationAt(it) }
    addItemDecoration(decoration)
}