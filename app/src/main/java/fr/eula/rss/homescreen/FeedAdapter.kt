package fr.eula.rss.homescreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.eula.rss.databinding.ListitemEmptyListBinding
import fr.eula.rss.databinding.ListitemPageTitleBinding
import fr.eula.rss.databinding.ListitemRssitemBinding
import fr.eula.rss.parser.RSSItem
import java.lang.Exception

/**
 * Items to be displayed in the RSS feed. Note that this contains both what should be displayed when
 * empty, and when there is data. This means that if no data is present, you should push to your
 * recyclerview a list with a single `Empty` inside so it can render it.
 *
 *  - Empty will take up the entire space and indicate to the user there is no data.
 *  - Title is a simple title taking up more space than a regular element
 *  - RSSItem are our actual items from the feed, although with fewer data than a full, proper RSSItem.
 */
sealed class FeedItem {
    object Empty : FeedItem()
    object Title : FeedItem()
    data class RSSItem(val id: String, val title: String, val imageUrl: String?, val summary: String) : FeedItem()
}

class FeedItemItemCallback : DiffUtil.ItemCallback<FeedItem>() {
    override fun areContentsTheSame(oldItem: FeedItem, newItem: FeedItem): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: FeedItem, newItem: FeedItem): Boolean {
        return (oldItem is FeedItem.Empty && newItem is FeedItem.Empty) ||
                (oldItem is FeedItem.Title && newItem is FeedItem.Title) ||
                (oldItem is FeedItem.RSSItem && newItem is FeedItem.RSSItem && oldItem.id == newItem.id)
    }
}

class FeedAdapter(private val onClickListener: (String) -> Unit, source: List<FeedItem> = listOf()) : ListAdapter<FeedItem, RecyclerView.ViewHolder>(FeedItemItemCallback()) {
    init {
        submitList(source)
    }

    override fun getItemCount(): Int = currentList.size

    override fun getItemViewType(position: Int): Int {
        return when (currentList[position]) {
            is FeedItem.Empty -> TYPE_EMPTY
            is FeedItem.Title -> TYPE_TITLE
            is FeedItem.RSSItem -> TYPE_RSSITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            TYPE_EMPTY -> {}
            TYPE_TITLE -> {}
            TYPE_RSSITEM -> (holder as RssItemFeedItemVH).bind(currentList[position] as FeedItem.RSSItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_EMPTY -> EmptyFeedItemVH(ListitemEmptyListBinding.inflate(inflater, parent, false))
            TYPE_TITLE -> TitleFeedItemVH(ListitemPageTitleBinding.inflate(inflater, parent, false))
            TYPE_RSSITEM -> RssItemFeedItemVH(ListitemRssitemBinding.inflate(inflater, parent, false), onClickListener)
            else -> throw Exception("unknown view type")
        }
    }

    companion object {
        private val TYPE_EMPTY = 0
        private val TYPE_TITLE = 1
        private val TYPE_RSSITEM = 2
    }
}

private class EmptyFeedItemVH(binding: ListitemEmptyListBinding) : RecyclerView.ViewHolder(binding.root)
private class TitleFeedItemVH(binding: ListitemPageTitleBinding) : RecyclerView.ViewHolder(binding.root)
private class RssItemFeedItemVH(val binding: ListitemRssitemBinding, val onClickListener: (String) -> Unit) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: FeedItem.RSSItem) {
        binding.listitemRssitemTitle.text = item.title
        binding.listitemRssitemSummary.text = item.summary
        item.imageUrl?.let { url ->
            Glide.with(binding.root)
                .load(url)
                .centerCrop()
                .into(binding.listitemRssitemImage)
        }

        // Performance of this is... not exceptional. But not necessarily an issue either.
        binding.root.setOnClickListener { onClickListener(item.id) }
    }
}