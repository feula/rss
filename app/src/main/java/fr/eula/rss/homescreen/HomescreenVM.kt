package fr.eula.rss.homescreen

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.*
import com.android.volley.Cache
import com.android.volley.RequestQueue
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.NoCache
import com.android.volley.toolbox.Volley
import fr.eula.rss.R
import fr.eula.rss.parser.NumeriquesRSSFetcher
import fr.eula.rss.parser.NumeriquesRSSTransformer
import fr.eula.rss.parser.RSSFetcher
import fr.eula.rss.parser.RSSSource
import fr.eula.rss.repository.Repository
import fr.eula.rss.utils.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomescreenVMFactory(private val source: RSSSource, private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(RSSSource::class.java, Repository::class.java)
            .newInstance(source, repository)
    }
}

class HomescreenVM(private val source: RSSSource, private val repository: Repository) : ViewModel() {
    private val _feedArticles = MutableLiveData<List<FeedItem>>()
    val feedArticles: LiveData<List<FeedItem>> = _feedArticles

    private val _userNotification = SingleLiveEvent<@StringRes Int>()
    val userNotification: LiveData<Int> = _userNotification

    init {
        _userNotification.postValue(R.string.loading_articles)
        _feedArticles.postValue(listOf(FeedItem.Empty))

        viewModelScope.launch {
            fetchArticles()
        }
    }


    private suspend fun fetchArticles() {
        val items = source.get()
        // We fetch the remote data and save it to database immediately, but we still use that fresh
        // data to display it to the user. The database mostly serves for details.
        if (items.isRight()) {
            repository.save((items as Either.Right).b)
        }
        val data = if (items.isLeft()) {
            // Let's attempt to load the data from cache, at least, and notify the user.
            coroutineScope {
                launch {
                    delay(100)
                    _userNotification.postValue(R.string.could_not_load_articles_falling_back)
                }
            }
            repository.getAll().right()
        } else items

        val output = data.fold(
            left = { listOf(FeedItem.Empty) },
            right = {
                // Always generate a title
                listOf(FeedItem.Title) + it.map { rss -> FeedItem.RSSItem(rss.id, rss.title, rss.imageUrl, rss.summary) }
            }
        )

        _feedArticles.postValue(output)

        // We introduce a bit of delay to prevent the notifications from being immediately dismissed.
        // Don't forget to launch this in a new coroutine if you need to do anything after.
        if (data.isLeft()) {
            delay(200L)
            _userNotification.postValue(R.string.could_not_load_articles)
        }
    }
}