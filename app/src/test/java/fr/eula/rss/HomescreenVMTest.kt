package fr.eula.rss

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.android.volley.VolleyError
import fr.eula.rss.homescreen.FeedItem
import fr.eula.rss.homescreen.HomescreenVM
import fr.eula.rss.parser.RSSItem
import fr.eula.rss.utils.left
import fr.eula.rss.utils.right
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.Rule
import org.junit.Test
import org.threeten.bp.ZonedDateTime

class HomescreenVMTest  {
    private val workingItem = RSSItem("Test item", "http://nope/", "1", ZonedDateTime.now(),
        "Summary", "Me", "This is the content of the article", "http://nope/item.jpg")

    @Rule
    @JvmField
    val instantTestRule = InstantTaskExecutorRule()

    init {
        Dispatchers.setMain(Dispatchers.Default)
    }

    @Test
    fun `the homescreen first starts up empty`() {
        val viewModel = HomescreenVM(mockk(relaxed = true), mockk(relaxed = true))
        assert(viewModel.feedArticles.getOrAwaitValue() == listOf(FeedItem.Empty))
    }

    @Test
    fun `the homescreen properly fetches items and displays them, starting with a title`() {
        val viewModel = HomescreenVM(mockk {
            every { runBlocking { get() } } returns listOf(workingItem).right()
        }, mockk(relaxed = true))

        viewModel.feedArticles.getOrAwaitValue() // Wait out one emission
        val data = viewModel.feedArticles.getOrAwaitValue()
        assert(data == listOf(FeedItem.Title, workingItem))
    }

    @Test
    fun `the homescreen notifies the user of loading and of a failure`() {
        val viewModel = HomescreenVM(mockk {
            every { runBlocking { get() } } returns mockk<VolleyError>().left()
        }, mockk(relaxed = true))

        assert(viewModel.userNotification.getOrAwaitValue() == R.string.loading_articles)
        assert(viewModel.userNotification.getOrAwaitValue() == R.string.could_not_load_articles)
    }
}