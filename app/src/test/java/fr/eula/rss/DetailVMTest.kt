package fr.eula.rss

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import fr.eula.rss.detail.DetailVM
import fr.eula.rss.parser.RSSItem
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.Rule
import org.junit.Test
import org.threeten.bp.ZonedDateTime
import java.util.concurrent.TimeoutException

class DetailVMTest {
    private val workingItem = RSSItem("Test item", "http://nope/", "1", ZonedDateTime.now(),
        "Summary", "Me", "This is the content of the article", "http://nope/item.jpg")

    @get:Rule
    val instantTestRule = InstantTaskExecutorRule()

    init {
        Dispatchers.setMain(Dispatchers.Default)
    }

    @Test
    fun `the viewmodel properly fetches the data and notifies the fragment`() {
        val viewModel = DetailVM("1", mockk {
            every { runBlocking { getOne("1") } } returns workingItem
        })

        assert(viewModel.article.getOrAwaitValue() == workingItem)
    }

    @Test
    fun `the viewmodel does not emit anything if there is no such article`() {
        val viewModel = DetailVM("1", mockk {
            every { runBlocking { getOne("1") } } returns null
        })

        val thrown = try {
            assert(viewModel.article.getOrAwaitValue() == null)
            false
        } catch (ex: TimeoutException) {
            true
        }
        if (!thrown) assert(false) { "The livedata should never have been set." }

        // We cannot unit test the SingleLiveEvent
    }
}