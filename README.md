# RSS

Un simple lecteur RSS pour le site Les numériques.

### Choix techniques

Processus de développement:

 1 - Analyse du format du schéma RSS renvoyé
 2 - Réalisation de mockups rapides pour obtenir un flow dans l'application et une idée du travail a effectuer
 3 - Mise en place des fragments de base avec données de test
 4 - Développement du parser RSS, test et attache au ViewModel afin de ne plus avoir de données de test
 5 - Mise en place de la base de données necessaire pour persister les éléments parsés et éviter d'avoir à les re-parser (ou bien de mettre tout l'élément dans un Bundle)
 6 - Finitions, test unitaires aux endroits ou cela est possible.

Dans une application de cette taille, les possibilités de tests unitaires sont minces: les ViewModels sont testables, mais les émissions sur les LiveData sont difficiles à tester. Les tests unitaires en place peuvent éventuellement échouer, en fonction de la vitesse d'émission de leurs valeurs. De la même façon, le parser RSS fait appel à XmlPullParser, qui n'est pas disponible lors des tests unitaires. Ceux ci sont donc testés manuellement, avec le plus de cas possibles.

Injection de dépendances
========================

Les dépendances sont passées manuellement, pour la plupart: l'activité contient le Repository (qui est un Singleton), et les dépendences sont crées lors de la création des ViewModel. Dans une application de plus grande taille (ou tout simplement ou nous savons que les fonctionnalités vont augmenter avec le temps), j'aurais mis en place un système d'injection de dépendances tels que Dagger ou Koin. Le cout pour les mettre en place dans une nouvelle application n'est pas élevé.

Cette inversion de controle, en passant les dépendances necessaires aux objets permet de tester leur comportement beaucoup plus facilement, ainsi que de le changer si besoin est, nos dépendances faisant références à de simples interfaces pour la plupart. Ainsi, lire un autre flux RSS nécessiterait seulement d'implémenter un nouveau RSSFetcher (ou bien d'écrire un fetcher générique prenant simplement une URL) et un RSSTransformer (dans ce cas ci, le parser écrit contient de légères spécificités au flux donné, notamment l'image de l'article, créer un parser générique réutiliserait néanmoins une grande partie du code.)


Parser XML
==========

Les outils pour parser le XML sous Android sont très limités, et ceux disponibles normalement dans la JVM comme Jackson, SAX, etc. permettant de déserialiser directement vers un objet ne le sont pas généralement sous Android. XmlPullParser nous permet de lire node par node les données, mais les possibilités d'erreurs lors du parsing sont grandes. Chaque méthode mise en place vérifie que nous ne sommes pas arrivés à la fin du document pour éviter de boucler infiniment.


Architecture utilisée
=====================

Modèle MVVM simple, une seule activité avec plusieurs fragments. Cela permet un passage de données plus simple d'un composant à un autre. Si les fragments prennent des paramètres (tel que DetailFragment), ceux ci sont gardés les plus simples possibles. Passer juste un ID d'article et aller le lire en base est plus simple à écrire et à faire évoluer que de passer l'intégralité des données dans un Bundle.

Base de données
===============

Garder les éléments RSS lus dans une base de données est plus simple que d'essayer de les garder en mémoire. Pour cela, SQLDelight est utilisé, permettant à mon gout une utilisation plus simple que Room (notamment, dans le cas d'un projet plus complexe, une gestion des jointures bien plus simple). Ces données sont mises à jour en base des que le flux est lu, et utilisées pour les détails.

Dans le cas d'une application plus complexe (notamment avec rafraichissement automatique des données), cette base de données elle même serait toujours la source de données (permettant ainsi d'afficher les changements en temps réel). Aujourd'hui, la base de données est utilisée en fallback lors d'un chargement échoué, mais son utilisation pour l'intégralité des données est simple à mettre en place.

Gestion des erreurs
===================

La gestion des erreurs dans l'application se fait via une implémentation simple du monad Either. C'est une simple structure de données pouvant contenir soit un type A ou B, permettant une distinction avec un seul type de retour. Son utilisation est pour moi plus simple que de gérer des exceptions, qui peuvent rendre le flux du code plus difficile à lire. Un Either force son utilisateur à spécifier les deux cas immédiatement et au même endroit, ou bien laisser le code appelant le spécifier.

Coroutines
==========

Les coroutines offertes par Kotlin sont utilisées pour le lancement de tâches de fond, comme la récupération des données, afin de laisser le thread de l'UI libre de travailler jusqu'a ce que des données soient recues. Volley effectue automatiquement les requetes HTTP sur un thread spécifique, mais les coroutines nous permettraient de spécifier manuellement `Dispatchers.IO` si besoin, pour éviter des connections sur le thread principal.

**Notes spécifiques a certains composants:**

listitem_page_title.xml
=======================

Dans une application plus grande, pourrait être réutilisé comme composant générique d'un RecyclerView, son adapter pourrait alors éventuellement prendre en paramètre un id de ressource à afficher en titre.

HomescreenVM.kt
===============

FeedItem est un sealed class afin de pouvoir spécifier exactement les éléments entrant dans notre recyclerview, et de bénéficier de vérifications par le compilateur. Idéalement, `FeedItem.Empty` pourrait être spécifié par un ADT (FeedItem | Empty) afin d'éviter les cas ou on ajouterait manuellement à la fois `Empty` et des éléments, mais Kotlin (ou Java) ne sont pas capables de gérer ça. Lorsque l'on a aucun élément, on s'assurera de toujours avoir `Empty` dans les éléments du recyclerview

FeedAdapter.kt
==============

Dans une application plus grande, idéalement, j'utiliserais Epoxy/Groupie/FastAdapter/AdapterDelegates/etc. afin de pouvoir réutiliser chaque type d'élément à plusieurs endroits. Dans celle ci, rien n'a besoin d'être réutilisé, donc on a une simple implémentation de plusieurs viewtypes dans le même adapter.


Temps passé: 8h
